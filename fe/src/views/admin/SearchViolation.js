import React from "react";

import CardViolationResult from "components/Cards/CardViolationResult";

const SearchViolation = () => {
    return ( 
        <>
            <div className="flex flex-wrap">
                <div className="w-full mb-2 px-4">
                    <CardViolationResult isLoggedIn={sessionStorage.getItem('isLoggedIn')}/>
                </div>
            </div>
        </>
     );
}
 
export default SearchViolation;