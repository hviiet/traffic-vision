import React from "react";

// components

import CardTable from "components/Cards/CardTable.js";

export default function Tables({ searchValue }) {
  return (
    <>
      <div className="flex flex-wrap mt-4">
        <div className="w-full mb-2 px-4">
          <CardTable searchValue={searchValue} />
        </div>
      </div>
    </>
  );
}
