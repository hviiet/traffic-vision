import React from "react";

const Streaming = () => {
    const [imageSrc, setImageSrc] = React.useState("");

    React.useEffect(() => {
        const interval = setInterval(() => {
            // setImageSrc(`https://m96fb7xd-8000.asse.devtunnels.ms/image/image.png?random=${Math.random()}`);
            setImageSrc(`http://192.168.1.8:8000/image/image.png?random=${Math.random()}`);
        }, 30);
        return () => clearInterval(interval);
    }, []);

    return ( 
        <>
            <div className="flex flex-wrap">
                <div className="w-full px-4">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-10 shadow-lg rounded-md">
                        <img height="60vh" width="100%" src={imageSrc} alt=""/>
                    </div>
                </div>
            </div>
        </>
     );
}
 
export default Streaming;
