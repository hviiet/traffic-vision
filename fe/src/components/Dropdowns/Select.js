const SelectLocation = ({ label_name, options, choice, onChange }) => {
    return ( 
        <label className="form-control w-full max-w-xs pr-4 md:pr-10 mr-auto">
            <div className="label">
                <span className="label-text text-white">{label_name}</span>
            </div>
            <select className="select select-bordered rounded" onChange={onChange}>
                <option disabled selected>Chọn</option>
                {options && options.map((option, index) => (
                  <option key={index} value={option}>{option}</option>
                ))} 
            </select>
        </label>
     );
}
 
export default SelectLocation;