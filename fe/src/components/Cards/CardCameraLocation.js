import React from 'react';
import Select from 'components/Dropdowns/Select';
import { getAllCamLocation } from 'services/camService';

const CardCameraLocation = () => {
    const [addresses, setAddresses] = React.useState([]);
    const [cities, setCities] = React.useState([]);
    const [selectedCity, setSelectedCity] = React.useState('');
    const [wards, setWards] = React.useState([]);
    const [selectedWard, setSelectedWard] = React.useState('');

    React.useEffect(() => {
        getAddresses();
    }, []);

    const getAddresses = async () => {
        let res = await getAllCamLocation();
        if (res && res.data) {
            setAddresses(res.data.violations.location);
        }
    }

    React.useEffect(() => {
        if (selectedCity) {
            let filteredWards = [];
            for (let i = 0; i < addresses.length; i++) {
                const address = addresses[i].address.split(" - ");
                if (address[1] === selectedCity && address[0]) {
                    filteredWards.push(address[0]);
                }
            }
            setWards(filteredWards);
        }
    }, [selectedCity, addresses]);

    const handleCityChange = (event) => {
        setSelectedCity(event.target.value);
    }

    const handleWardChange = (event) => {
        setSelectedWard(event.target.value);
    }
    
    for (let i = 0; i < addresses.length; i++) {
        const address = addresses[i].address.split(" - ");
        if (!cities.includes(address[1]) && address[1]) {
            setCities([...cities, address[1]]);
        }
    }

    return ( 
        <div className="flex items-end flex-wrap">
            <Select label_name="Tỉnh/ Thành phố" options={cities} onChange={handleCityChange} />
            <Select label_name="Huyện/ Quận" options={wards} onChange={handleWardChange} />

            <button className="btn btn-active btn-neutral">Tìm kiếm</button>
        </div>
     );
}
 
export default CardCameraLocation;