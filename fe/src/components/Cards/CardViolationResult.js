import React from "react";
import PropTypes from "prop-types";
import ReactPaginate from "react-paginate";
import { searchLicensePlate } from "services/violatorService";
// components

import CardModal from "components/Cards/CardModal.js";

export default function CardTable({ color, isLoggedIn }) {
    const [violators, setViolators] = React.useState([]);
    const [totalPages, setTotalPages] = React.useState(1);
    const [searchValue, setSearchValue] = React.useState('');


    const getLicensePlateResult = async (page, licensePlate) => {
        try {
            let res = await searchLicensePlate(page, licensePlate);
            if (res && res.data) {
                if (licensePlate === '' && isLoggedIn === null) return setViolators([]);
                setViolators(res.data.violations);
                setTotalPages(res.data.total_pages);
            }
        } catch (error) {
            console.error('There was an error!', error);
        }
    }


    const HandleSearchViolation = () => {
        getLicensePlateResult(1, searchValue);
    }   

    const handlePageClick = async (event) => {
        getLicensePlateResult(event.selected + 1, '');
      }
    

    const formatDate = (violation_time) => {
        const date = new Date(violation_time);
        const formattedDate = 
        ("0" + date.getDate()).slice(-2) + "/" +
        ("0" + (date.getMonth() + 1)).slice(-2) + "/" +
        date.getFullYear() + " " +
        
        ("0" + date.getHours()).slice(-2) + ":" +
        ("0" + date.getMinutes()).slice(-2) + ":" +
        ("0" + date.getSeconds()).slice(-2);
        return formattedDate;
    }


    return (
        <>
            <div className="mb-7 flex items-end">
                <div className="relative flex w-1/2 items-stretch">
                    <span className="z-10 h-full leading-snug font-normal absolute text-center text-blueGray-300 absolute bg-transparent rounded text-base items-center justify-center w-8 pl-3 py-3">
                        <i className="fas fa-search"></i>
                    </span>
                    <input
                        type="text"
                        placeholder="Tìm biển số xe..."
                        className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:ring w-full pl-10"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                    />
                    <button className="btn btn-active btn-neutral ml-2" onClick={HandleSearchViolation}>Kiểm tra</button>
                </div>

            </div>

            <div
                className={
                "relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded " +
                (color === "light" ? "bg-white" : "bg-lightBlue-900 text-white")
                }
            >
                <div className="rounded-t mb-0 px-4 py-3 border-0">
                    <div className="flex flex-wrap items-center">
                        <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                            <h3
                                className={
                                "font-semibold text-lg " +
                                (color === "light" ? "text-blueGray-700" : "text-white")
                                }
                            >
                                Chi tiết vi phạm
                            </h3>
                        </div>
                    </div>
                </div>

                {violators.length === 0 ? (<div className="flex flex-col justify-center items-center w-full h-96 overflow-x-auto">
                    <i className="far fa-times-circle text-7xl text-red-500"></i>
                    <h1 className="text-gray-400 text-3xl">Không tìm thấy kết quả</h1>
                </div>) : (
                    <div className="block w-full overflow-x-auto">

                        <table className="items-center w-full bg-transparent border-collapse ">
                            <thead className="top-0">
                            <tr>
                                <th
                                className={
                                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                                    (color === "light"
                                    ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                                    : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                                }
                                >
                                Loại phương tiện
                                </th>
                                <th
                                className={
                                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                                    (color === "light"
                                    ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                                    : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                                }
                                >
                                Biển số xe
                                </th>
                                <th
                                className={
                                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                                    (color === "light"
                                    ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                                    : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                                }
                                >
                                Thời gian vi phạm
                                </th>
                                <th
                                className={
                                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                                    (color === "light"
                                    ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                                    : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                                }
                                >
                                Địa điểm vi phạm
                                </th>
                                <th
                                className={
                                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                                    (color === "light"
                                    ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                                    : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                                }
                                >
                                Hình ảnh vi phạm
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            {violators.map((violator, index) => {
                                return (
                                <tr key={`violator-${index}`}>
                                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                    {violator.violator.vehicle_type}
                                    </td>
                                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                    {violator.violator.vehicle_plate}
                                    </td>
                                    
                                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                    <div className="flex items-center">
                                        {formatDate(violator.violation_time)}
                                    </div>
                                    </td>
                                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                    {violator.location.address}
                                    </td>
                                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-md whitespace-nowrap p-4">
                                    <div className="flex items-center">
                                        <button className="btn btn-ghost" onClick={()=>document.getElementById(`${violator.violator.vehicle_plate}`).showModal()}>chi tiết</button>
                                    </div>
                                    </td>
                                    <CardModal vehiclePlate={violator.violator.vehicle_plate} imgPath={violator.violation_image_path}/>
                                
                                </tr>
                                )
                            })}
                            </tbody>

                        </table>

                    </div>
                )}
                
            </div>
            
            {
                isLoggedIn && (
                    <div className="w-full mb-12 px-4 flex justify-end items-center">
                        <ReactPaginate
                        breakLabel="..."
                        nextLabel="next >"
                        onPageChange={handlePageClick}
                        pageRangeDisplayed={5}
                        pageCount={totalPages}
                        previousLabel="< previous"
        
                        pageClassName="join-item btn"
                        previousClassName="join-item btn"
                        nextClassName="join-item btn"
                        containerClassName="join"
                        activeClassName="btn-active"
                        />
                    </div>
                )
            }
        </>
    );
}

CardTable.defaultProps = {
  color: "light",
};

CardTable.propTypes = {
  color: PropTypes.oneOf(["light", "dark"]),
};
