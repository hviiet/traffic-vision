import React from "react";
import PropTypes from "prop-types";
import ReactPaginate from "react-paginate";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import { getAllViolators } from "services/violatorService";
// components

import CardModal from "components/Cards/CardModal.js";

export default function CardTable({ color, searchValue }) {
  const [violators, setViolators] = React.useState([]);
  const [totalPages, setTotalPages] = React.useState(1);
  const [highlightedRecord, setHighlightedRecord] = React.useState(null);

  const client = new W3CWebSocket('ws://127.0.0.1:8000/ws/');
  // const client = new W3CWebSocket('ws://192.168.149.78:8000/ws/');

  React.useEffect(() => {
    getViolators(1);
  }, [])
  
  React.useEffect(() => {
    client.onopen = () => {
      console.log('WebSocket Client Connected');
    };
    client.onmessage = (message) => {
      const dataFromServer = JSON.parse(message.data);
      console.log(dataFromServer);
      if (dataFromServer['type'] === 'on_connect') {
        client.send(JSON.stringify({
          connect_type: 'client',
        }));   
      }
      else {
        setViolators((prevViolators) => {
          let newViolators = [dataFromServer, ...prevViolators];
          
          if (newViolators.length > 10) {
            newViolators = newViolators.slice(0, 10);
          }

          setHighlightedRecord(dataFromServer);

          return newViolators;
        });
      }
    };
  }, [])

  React.useEffect(() => {
    if (highlightedRecord) {
      setTimeout(() => {
        setHighlightedRecord(null);
      }, 1000);
    }
  }, [highlightedRecord]);


  const getViolators = async (page) => {
    try {
      let res = await getAllViolators(page);
      if (res && res.data) {
        setViolators(res.data.violations);
        setTotalPages(res.data.total_pages);
      }
    } catch (error) {
      console.error('There was an error!', error);
    }
  }

  const formatDate = (violation_time) => {
    const date = new Date(violation_time);
    const formattedDate = 
    ("0" + date.getDate()).slice(-2) + "/" +
    ("0" + (date.getMonth() + 1)).slice(-2) + "/" +
    date.getFullYear() + " " +
    
    ("0" + date.getHours()).slice(-2) + ":" +
    ("0" + date.getMinutes()).slice(-2) + ":" +
    ("0" + date.getSeconds()).slice(-2);
    return formattedDate;
  }

  return (
    <>
      <div
        className={
          "relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded " +
          (color === "light" ? "bg-white" : "bg-lightBlue-900 text-white")
        }
      >
        <div className="rounded-t mb-0 px-4 py-3 border-0">
          <div className="flex flex-wrap items-center">
            <div className="relative w-full px-4 max-w-full flex-grow flex-1">
              <h3
                className={
                  "font-semibold text-lg " +
                  (color === "light" ? "text-blueGray-700" : "text-white")
                }
              >
                Chi tiết vi phạm
              </h3>
            </div>
          </div>
        </div>
        <div className="block w-full overflow-x-auto">
          {/* Projects table */}
          <table className="items-center w-full bg-transparent border-collapse ">
            <thead className="top-0">
              <tr>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Loại phương tiện
                </th>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Biển số xe
                </th>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Thời gian vi phạm
                </th>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Địa điểm vi phạm
                </th>
                <th
                  className={
                    "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left " +
                    (color === "light"
                      ? "bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      : "bg-lightBlue-800 text-lightBlue-300 border-lightBlue-700")
                  }
                >
                  Hình ảnh vi phạm
                </th>
              </tr>
            </thead>

            <tbody>
              {violators.map((violator, index) => {
                return (
                  <tr key={`violator-${index}`} className={violator === highlightedRecord ? 'bg-base-200' : ''}>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {violator.violator.vehicle_type}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {violator.violator.vehicle_plate}
                    </td>
                    
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      <div className="flex items-center">
                        {formatDate(violator.violation_time)}
                      </div>
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {violator.location.address}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-md whitespace-nowrap p-4">
                      <div className="flex items-center">
                        <button className="btn btn-ghost" onClick={()=>document.getElementById(`${violator.violator.vehicle_plate}`).showModal()}>chi tiết</button>
                      </div>
                    </td>
                    <CardModal vehiclePlate={violator.violator.vehicle_plate} imgPath={violator.violation_image_path}/>
                  
                  </tr>
                )
              })}
            </tbody>

          </table>

        </div>
      </div>

      {/* <div className="w-full mb-12 px-4 flex justify-end items-center">
        <ReactPaginate
          breakLabel="..."
          nextLabel="next >"
          onPageChange={handlePageClick}
          pageRangeDisplayed={5}
          pageCount={totalPages}
          previousLabel="< previous"

          pageClassName="join-item btn"
          previousClassName="join-item btn"
          nextClassName="join-item btn"
          containerClassName="join"
          activeClassName="btn-active"
        />
      </div> */}
    </>
  );
}

CardTable.defaultProps = {
  color: "light",
};

CardTable.propTypes = {
  color: PropTypes.oneOf(["light", "dark"]),
};
