import React from "react";
import { useLocation } from "react-router-dom";

// components

import CardStats from "components/Cards/CardStats.js";
import CardCameraLocation from "components/Cards/CardCameraLocation";

export default function HeaderStats() {
  const location = useLocation();
  const cardStats = location.pathname === '/admin/tables' || location.pathname === '/guest/tables' || location.pathname === '/admin/searchViolation' || location.pathname === '/guest/searchViolation';
  const isStreaming = location.pathname === '/admin/streaming' || location.pathname === '/guest/streaming' || location.pathname === '/';

  return (
    <>
      {/* Header */}

      <div className={`relative ${cardStats ? 'bg-lightBlue-600 pt-32 pb-20' : 'bg-lightBlue-600 md:pt-32 pb-32 pt-12'}`}>
        <div className="px-4 md:px-10 mx-auto w-full">
          <div>
            {!cardStats && (
              <div className="flex flex-wrap">
                {/* <div className={`w-full ${isStreaming ? 'lg:w-8/12 xl:w-8/12 px-4' : 'lg:w-6/12 xl:w-6/12 px-4'}`}>
                  {isStreaming ? (<CardCameraLocation />) : (
                    <CardStats
                    statSubtitle="Số lượt vi phạm trong tháng"
                    statTitle="350,897"
                    statArrow="up"
                    statPercent="3.48"
                    statPercentColor="text-emerald-500"
                    statDescripiron="Since last month"
                    statIconName="far fa-chart-bar"
                    statIconColor="bg-red-500"
                  />
                  )} 
                </div> */}

                {!isStreaming && (
                  <div className="w-full lg:w-6/12 xl:w-6/12 px-4">
                    <CardStats
                      statSubtitle="Số lượt vi phạm trong tháng"
                      statTitle="350,897"
                      statArrow="up"
                      statPercent="3.48"
                      statPercentColor="text-emerald-500"
                      statDescripiron="Since last month"
                      statIconName="far fa-chart-bar"
                      statIconColor="bg-red-500"
                    />
                </div>
                )}
                {!isStreaming && (
                  <div className="w-full lg:w-6/12 xl:w-6/12 px-4">
                    <CardStats
                      statSubtitle="Số lượt vi phạm trong năm"
                      statTitle="2,356"
                      statArrow="down"
                      statPercent="3.48"
                      statPercentColor="text-red-500"
                      statDescripiron="Since last week"
                      statIconName="fas fa-chart-pie"
                      statIconColor="bg-orange-500"
                    />
                </div>
                )}
                
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
