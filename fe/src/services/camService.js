import axios from "./custom-axios";

const getAllCamLocation = () => {
    return axios.post("/violations/", { page: 1 });
}

const getSelectedCam = (location) => {
    return axios.get(`/violators?address=${location}`);
}

export { getAllCamLocation, getSelectedCam };