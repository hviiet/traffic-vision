import React from 'react';
import { Switch, Redirect } from 'react-router-dom';

// components
import Sidebar from 'components/Sidebar/Sidebar';
import AdminNavbar from 'components/Navbars/AdminNavbar';
import HeaderStats from 'components/Headers/HeaderStats';

// views
import Dashboard from 'views/admin/Dashboard';
import Maps from 'views/admin/Maps';
import Settings from 'views/admin/Settings.js';
import Tables from 'views/admin/Tables.js';
import Streaming from 'views/admin/Streaming';
import SearchViolation from 'views/admin/SearchViolation';

// authentication
import ProtectedRoute from 'authentication/loggin';

export default function Admin() {
  const [searchValue, setSearchValue] = React.useState('');

  return (
    <>
      <Sidebar />
      <div className="relative md:ml-64 bg-blueGray-100">
        <AdminNavbar isLoggedIn={true} onSearchChange={setSearchValue}/>
        {/* Header */}
        <HeaderStats />
        <div className="px-4 md:px-10 mx-auto w-full -m-24">
          <Switch>
            <ProtectedRoute path="/admin/dashboard" exact component={Dashboard} />
            <ProtectedRoute path="/admin/maps" exact component={Maps} />
            <ProtectedRoute path="/admin/settings" exact component={Settings} />
            <ProtectedRoute path="/admin/tables" exact render={() => <Tables searchValue={searchValue} />} />
            <ProtectedRoute path="/admin/searchViolation" exact component={SearchViolation} />
            <ProtectedRoute path="/admin/streaming" exact component={Streaming} />
            <Redirect from="/admin" to="/admin/dashboard" />
          </Switch>
        </div>
      </div>
    </>
  );
}