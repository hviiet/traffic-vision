import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

// components

import AdminNavbar from "components/Navbars/AdminNavbar.js";
import HeaderStats from "components/Headers/HeaderStats.js";
import GuestSidebar from "components/Sidebar/GuestSidebar";

// views

import Streaming from "views/admin/Streaming";
import SearchViolation from "views/admin/SearchViolation";

export default function Guest() {
  
  return (
    <>
      <GuestSidebar />
      <div className="relative md:ml-64 bg-blueGray-100">
        <AdminNavbar isLoggedIn={false}/>
        {/* Header */}
        <HeaderStats />
        <div className="px-4 md:px-10 mx-auto w-full -m-24">
          <Switch>
            <Route path="/guest/searchViolation" exact component={SearchViolation} />
            <Route path="/guest/streaming" exact component={Streaming} />

            <Route path="/" exact component={Streaming} />
            <Redirect from="/" to="/streaming" />
          </Switch>
        </div>
      </div>
    </>
  );
}
