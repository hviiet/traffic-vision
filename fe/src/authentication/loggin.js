import React from 'react';
import { Route, Redirect } from 'react-router-dom';


const ProtectedRoute = ({ component: Component, render: Render, ...rest }) => {
    const isLoggedIn = sessionStorage.getItem('isLoggedIn');

    return (
        <Route
            {...rest}
            render={props =>
                isLoggedIn === 'true' ? (
                    Render ? <Render {...props} /> : <Component {...props} />
                ) : (
                    <Redirect to="/login" />
                )
            }
        />
    );
};
export default ProtectedRoute;