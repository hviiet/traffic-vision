import numpy as np
import math
import cv2
import matplotlib.pyplot as plt

def rotate_image(image: np.ndarray, angle: float) -> np.ndarray:
    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result

def compute_skew(src_img: np.ndarray) -> float:
    if len(src_img.shape) == 3:
        h, w, _ = src_img.shape
    elif len(src_img.shape) == 2:
        h, w = src_img.shape
    else:
        print('unsupported image type')
        return 0.0
    img = cv2.medianBlur(src_img, 3)
    edges = cv2.Canny(img, threshold1 = 30, threshold2 = 100, apertureSize = 3, L2gradient = True)
    lines = cv2.HoughLinesP(edges, 1, math.pi/180, 30, minLineLength=w / 4.0, maxLineGap=h/4.0)
    angle = 0.0
    nlines = lines.size
    cnt = 0
    # show lines
    for x1, y1, x2, y2 in lines[0]:
        cv2.line(src_img, (x1, y1), (x2, y2), (0, 255, 0), 1)
    # cv2.imshow('lines', src_img)
    # cv2.waitKey(0)
    for x1, y1, x2, y2 in lines[0]:
        ang = np.arctan2(y2 - y1, x2 - x1)
        if math.fabs(ang) <= 30: # excluding extreme rotations
            angle += ang
            cnt += 1
    if cnt == 0:
        return 0.0
    return (angle / cnt)*180/math.pi

def deskew(src_img: np.ndarray) -> np.ndarray:
    return rotate_image(src_img, compute_skew(src_img))

def enhance(img: np.ndarray):
    # apply grayscale
    # if len(img.shape) == 3:
    #     img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # resize height to 200
    h, w, _ = img.shape
    aspect_ratio = w/h
    new_h = 200
    new_w = int(aspect_ratio * new_h)
    img = cv2.resize(img, (new_w, new_h))

    # enhance contrast with top hat and black hat
    kernel = np.ones((3, 3), np.uint8)
    tophat = cv2.morphologyEx(img, cv2.MORPH_TOPHAT, kernel)
    blackhat = cv2.morphologyEx(img, cv2.MORPH_BLACKHAT, kernel)
    img = cv2.add(img, tophat)
    img = cv2.subtract(img, blackhat)
    return img