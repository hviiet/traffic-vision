"""
This class will find four vertices of a rectangle in a given image contains a picture of tilted rectangle in 3D space.
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt
import itertools

def read_img(path: str) -> np.ndarray:
    """
    Read the image and return the numpy array of the image.
    """
    path = "sign__test\\" + path
    img = cv2.imread(path)
    return img

def preprocess(img: np.ndarray, plot: bool) -> np.ndarray:
    """
    Preprocess the image to make it easier to find the vertices of rectangle.
    """
    
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (5, 5), 0)
    edges = cv2.Canny(blur, 50, 150)
    edges = cv2.dilate(edges, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
    
    # plot
    if plot:
        plt.imshow(edges, cmap='gray')
        plt.show()
    return edges

def extract_convex(img: np.ndarray, edges: np.ndarray, plot: bool) -> np.ndarray:
    # Blank canvas.
    con = np.zeros_like(img)
    # Finding contours for the detected edges.
    contours, hierarchy = cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    # Keeping only the largest detected contour.
    page = sorted(contours, key=cv2.contourArea, reverse=True)[:5]
    print(page)
    con = cv2.drawContours(con, page, -1, (0, 255, 255), 3)
    # plot
    if plot:
        plt.imshow(con)
        plt.show()
        
    # Blank canvas.
    con = np.zeros_like(img)
    # Loop over the contours.
    list_corners = []
    for c in page:
        # Approximate the contour.
        epsilon = 0.02 * cv2.arcLength(c, True)
        list_corners.append(cv2.approxPolyDP(c, epsilon, True))
    hull_areas = []
    alike_score = []
    for corners in list_corners:
        # cv2.drawContours(con, c, -1, (0, 255, 255), 3)
        cv2.drawContours(con, corners, -1, (0, 255, 0), 10)
        # Sorting the corners and converting them to desired shape.
        corners = sorted(np.concatenate(corners).tolist())
        hull = cv2.convexHull(np.array(corners), False)
        hull_area = cv2.contourArea(hull)
        hull_areas.append(hull_area)
        alike_score.append(rectangle_score(corners, reduce_corners(corners)))
        print(hull_area, alike_score[-1] * 100)
        # Displaying the corners.
        for index, c in enumerate(corners):
            character = chr(65 + index)
            cv2.putText(con, character, tuple(c), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1, cv2.LINE_AA)
        if plot:
            plt.imshow(con)
            plt.show()
            # reset the canvas.
            con = np.zeros_like(img)
    # Finding the largest rectangle.
    hull_indx = np.argmax(hull_areas)
    alike_indx = np.argmax(alike_score)
    # extracted_corners = list_corners[hull_indx]
    extracted_corners = list_corners[alike_indx]
    # Sorting the corners and converting them to desired shape.
    corners = sorted(np.concatenate(extracted_corners).tolist())
    hull = cv2.convexHull(np.array(corners), False)
    # Displaying the corners.
    if plot:
        for index, c in enumerate(corners):
            character = chr(65 + index)
            cv2.putText(con, character, tuple(c), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1, cv2.LINE_AA)
        plt.imshow(con)
        plt.show()
    
    return np.array(corners)

def reduce_corners(corners: np.ndarray) -> np.ndarray:
    MAX = 1000000
    # find four corners
    tl = [MAX, MAX]
    tr = [0, MAX]
    br = [0, 0]
    bl = [MAX, 0]
    for corner in corners:
        if corner[0] + corner[1] < tl[0] + tl[1]:
            tl = corner
        if corner[0] - corner[1] > tr[0] - tr[1]:
            tr = corner
        if corner[0] + corner[1] > br[0] + br[1]:
            br = corner
        if corner[0] - corner[1] < bl[0] - bl[1]:
            bl = corner
    return np.array([tl, tr, br, bl])

def add_margin(corners: np.ndarray, height: int, width: int, ratio: float) -> np.ndarray:
    rh = int(ratio * height)
    rw = int(ratio * width)
    [tl, tr, br, bl] = corners
    tl = [tl[0] - rw, tl[1] - rh]
    tr = [tr[0] + rw, tr[1] - rh]
    br = [br[0] + rw, br[1] + rh]
    bl = [bl[0] - rw, bl[1] + rh]
    # check if the corners are within the image.
    tl[0] = max(0, tl[0])
    tl[1] = max(0, tl[1])
    tr[0] = min(width, tr[0])
    tr[1] = max(0, tr[1])
    br[0] = min(width, br[0])
    br[1] = min(height, br[1])
    bl[0] = max(0, bl[0])
    bl[1] = min(height, bl[1])
    return np.array([tl, tr, br, bl])

def find_destination_corners(img: np.ndarray, corners: np.ndarray, plot: bool) -> np.ndarray:
    [tl, tr, br, bl] = corners
    # add margin
    height, width, _ = img.shape
    corners = add_margin(corners, height, width, 0.02)
    # Finding the maximum width.
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    # Finding the maximum height.
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    # Final destination co-ordinates.
    destination_corners = [[0, 0], [maxWidth, 0], [maxWidth, maxHeight], [0, maxHeight]]
    
    # Getting the homography.
    M = cv2.getPerspectiveTransform(np.float32(corners), np.float32(destination_corners))
    # Perspective transform using homography.
    rot = np.zeros_like(img)
    print(destination_corners)
    final = cv2.warpPerspective(img, M, (destination_corners[2][0], destination_corners[2][1]), flags=cv2.INTER_LINEAR, dst=rot)
    if plot:
        plt.imshow(final)
        plt.show()
    
# TODO: Implement this function.
def rectangle_score(corners: np.ndarray, quad_corners: np.ndarray) -> float:
    """
    This function will return the score of the rectangle based on the angle it creates.
    Range: 0 to 1
    """
    score = 0.0
    full_area = cv2.contourArea(cv2.convexHull(np.array(corners), False))
    quad_area = cv2.contourArea(cv2.convexHull(np.array(quad_corners), False))
    try:
        score = quad_area / full_area
    except:
        return score
    return score
    

def main(file_name: str) -> np.ndarray:
    """
    Main function to find the four vertices of rectangle.
    """
    img = read_img(file_name)
    (dimx, dimy, _) = img.shape
    edges = preprocess(img, True)
    convex = extract_convex(img, edges, True)
    convex = reduce_corners(convex)
    # plot the corners
    for corner in convex:
        cv2.circle(img, tuple(corner), 10, (0, 0, 255), -1)
    plt.imshow(img)
    plt.show()
    streched = find_destination_corners(img, convex, True)
    return edges

if __name__ == "__main__":
    main("s13.png")
