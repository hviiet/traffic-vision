import time
import os
import pytesseract as pyt
import numpy as np
import cv2
from PIL import Image
import Rotater as rt
import matplotlib.pyplot as plt


class Reader:
    WHITE_LIST = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-."
    TESSERACT_PATH = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"
    ALP_CONFIG = r'-c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZ.- -l eng --oem 3 --psm 6 language_model_penalty_non_freq_dict_word=1 -c language_model_penalty_non_dict_word=1 -c load_system_dawg=0'
    NUM_CONFIG = r'-c tessedit_char_whitelist=0123456789.- -l eng --oem 3 --psm 6 language_model_penalty_non_freq_dict_word=1 -c language_model_penalty_non_dict_word=1 -c load_system_dawg=0'
    FUL_CONFIG = r'-c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ.- -l eng --oem 3 --psm 6 language_model_penalty_non_freq_dict_word=1 -c language_model_penalty_non_dict_word=1 -c load_system_dawg=0'

    pyt.pytesseract.tesseract_cmd = TESSERACT_PATH

    def __init__(self):
        print("Initialized Reader")

    def set_path(path):
        if os.path.exists:
            Reader.TESSERACT_PATH = path
            pyt.pytesseract.tesseract_cmd = path
        else:
            print("Path does not exist")

    def get_path():
        return Reader.TESSERACT_PATH

    def read(self, img: np.ndarray, plot=False):
        # img = rt.tune(img)
        if plot:
            plt.imshow(img, cmap="gray")
            plt.show()
        if len(img.shape) > 2:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        if plot:
            plt.imshow(img, cmap="gray")
            plt.show()
        h, w = img.shape
        img = cv2.resize(img, (int(w * 500 / h), 500))

        # Threshold the image to binary
        img = rt.maximizeContrast(img)
        if plot:
            plt.imshow(img, cmap="gray")
            plt.show()
        # img = cv2.GaussianBlur(img, (3, 3), 0)
        if plot:
            plt.imshow(img, cmap="gray")
            plt.show()
        _, img = cv2.threshold(
            img, 150, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        img = 255 - img
        if plot:
            print("BINARY")
            plt.imshow(img, cmap="gray")
            plt.show()
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
        if plot:
            print("Opening")
            plt.imshow(img, cmap="gray")
            plt.show()

        gray_pil = Image.fromarray(img)

        # apply canny and contour to border the character
        # img_canny = cv2.Canny(img, 50, 150)
        # if plot:
        #     plt.imshow(img_canny)
        #     plt.show()
        # contours, hierarchy = cv2.findContours(
        #     img_canny, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

        # boundRect = [None] * len(contours)
        # countours_polies = [None] * len(contours)
        # for i, c in enumerate(contours):
        #     contours_poly = cv2.approxPolyDP(c, 3, True)
        #     boundRect[i] = cv2.boundingRect(contours_poly)
        #     countours_polies[i] = contours_poly
        # img = remove_unwanted(img, boundRect, countours_polies)

        # for i in range(len(contours)):
        # 	cv2.rectangle(img, (int(boundRect[i][0]), int(boundRect[i][1])),
        # 				(int(boundRect[i][0] + boundRect[i][2]), int(boundRect[i][1] + boundRect[i][3])),
        # 				(128, 128, 128), 2)
        # cv2.imshow("Image", img)
        # cv2.waitKey(0)

        # Use pytesseract to read the image
        if plot:
            points = pyt.image_to_boxes(gray_pil, config=Reader.FUL_CONFIG)
        text = pyt.image_to_string(gray_pil, config=Reader.FUL_CONFIG)

        # Draw the boxes around the text
        if plot:
            for point in points.splitlines():
                point = point.split(' ')
                x, y, w, h = int(point[1]), int(
                    point[2]), int(point[3]), int(point[4])
                cv2.rectangle(
                    img, (x, img.shape[0]-y), (w, img.shape[0]-h), (255, 255, 255), 1)

            # Display the image
            cv2.imshow("Image", img)
            cv2.waitKey(0)
        if (text == ""):
            return "[UNKN]: No text detected"
        text = text.replace("\n", "-")
        if (text[-1] == "-"):
            text = text[:-1]
        return text.replace("\n", "-")


def remove_unwanted(img: np.ndarray, rectangles, contours_polies):
    img_area = img.shape[0] * img.shape[1]
    img_c = img.copy()
    for i in range(len(rectangles)):
        # criteria: if <2% area, remove, if 5%>area>=2% and height/width in [0.8, 1.25], if >5% area and height/width in [0.8, 4.7]
        x, y, w, h = rectangles[i]
        area = w * h
        perc_area = area / img_area
        print(str(perc_area * 100)[:4] + "% " + str(h/w)[:3], end=" ")

        # if (perc_area < 0.005):
        # 	img = turn_black(img, rectangles[i], contours_polies[i])
        # 	print("remove1")
        # elif (perc_area < 0.02 and not(h/w < 1.4 and h/w > 0.71)):
        # 	img = turn_black(img, rectangles[i], contours_polies[i])
        # 	print("remove2")
        # elif perc_area >= 0.03 and not (h/w < 4.7 and h/w > 0.8):
        # 	img = turn_black(img, rectangles[i], contours_polies[i])
        # 	print("remove3")
        keep = False
        if (perc_area > 0.003 and perc_area < 0.007) and (h/w > 0.71 and h/w < 1.4):
            keep = True
        if (perc_area >= 0.007 and perc_area < 0.15) and (h/w > 0.8 and h/w < 4.7):
            keep = True
        if keep:
            cv2.rectangle(img_c, (int(rectangles[i][0]), int(rectangles[i][1])),
                          (int(rectangles[i][0] + rectangles[i][2]),
                           int(rectangles[i][1] + rectangles[i][3])),
                          (128, 128, 128), 2)
        cv2.imshow("Image", img_c)
        cv2.waitKey(0)
    return img


def turn_black(img, rectangle, contours_poli):
    for x in range(rectangle[0], rectangle[0] + rectangle[2]):
        for y in range(rectangle[1], rectangle[1] + rectangle[3]):
            change = False if point_position(
                contours_poli, x, y) == "outside" else True
            if (change):
                img[y, x] = np.array([0], dtype=np.uint8)
    return img


def point_position(contour, x, y):
    """
    Determines the position of a point (x, y) relative to a contour.

    Args:
            contour (numpy.ndarray): The contour (output from cv2.approxPolyDP()).
            x (int): X-coordinate of the point.
            y (int): Y-coordinate of the point.

    Returns:
            str: 'inside', 'outside', or 'border' based on the point's position.
    """
    dist = cv2.pointPolygonTest(contour, (x, y), True)

    if dist > 0:
        return 'inside'
    elif dist < 0:
        return 'outside'
    else:
        return 'border'


# main
if __name__ == "__main__":
    start = time.time()
    reader = Reader()
    text = reader.read(cv2.imread("sign__test\\s23.png"), plot=True)
    print("SCAN RESULT:", text)
    print("Time taken: ", time.time() - start)
