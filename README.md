
# Traffic Vision


## How to run:
- For server:
```bash
    cd be
    py manage.py runserver
```
- To add migration and update database:
```bash
    cd be
    py manage.py makemigrations
    py manage.py migrate
```
- For ui:
```bash
    cd fe
    npm run start
```

    