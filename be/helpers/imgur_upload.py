import base64
import requests, queue
import aiohttp

async def a_upload_image_from_local_path(image_path):
    files = {'image': open(image_path, 'rb')}
    async with aiohttp.ClientSession() as session:
        async with session.post("https://api.imgur.com/3/image", data=files, headers={'Authorization': 'Client-ID clientid'}) as response:
            data = await response.json()
            return data.get('data', {}).get('link')


def upload_image_from_local_path(image_path):
    files=[
        ('image',('GHJQTpX.jpeg',open(image_path,'rb'),'image/jpeg'))
    ]
    headers = {
        'Authorization': 'Client-ID 51337f4bd122436'
    }

    response = requests.request("POST", "https://api.imgur.com/3/image", headers=headers,  files=files)

    url = response.json()["data"]["link"]
    return url

def upload_image_from_bytes(bytes_data):
    files=[
        ('image',('GHJQTpX.jpeg',bytes_data,'image/jpeg'))
    ]
    headers = {
        'Authorization': 'Client-ID 51337f4bd122436'
    }

    response = requests.request("POST", "https://api.imgur.com/3/image", headers=headers,  files=files)

    url = response.json()["data"]["link"]
    return url

class ImgurUpload:
    def __init__(self):
        self.url = "https://api.imgur.com/3/image"
        self.headers = {"Authorization ": "Client-ID 51337f4bd122436"}

    def upload_image_from_image_path(self, image_path):
        with open(image_path, "rb") as file:
            data = file.read()
            base64_data = base64.b64encode(data)

        # payload={'type': 'image',
        # 'title': 'Simple upload',
        # 'description': 'This is a simple image upload in Imgur'}
        files=[
            ('image',('GHJQTpX.jpeg',open(image_path,'rb'),'image/jpeg'))
        ]
        headers = {
            'Authorization': 'Client-ID 51337f4bd122436'
        }

        response = requests.request("POST", self.url, headers=headers,  files=files)


        url = response.json()["data"]["link"]
        return url 
    
    def upload_image_from_file(self, file):
        data = file.read()
        base64_data = base64.b64encode(data)
        response = requests.post(self.url, headers=self.headers, data={"image": base64_data})
        url = response.json()["data"]["link"]
        return url
    
    def upload_image_from_base64(self, base64_data):
        response = requests.post(self.url, headers=self.headers, data={"image": base64_data})
        url = response.json()["data"]["link"]
        return url
    
# imgur = ImgurUpload()
# url = upload_image_from_local_path("C:\\Users\\nviet\\Destop\\traffic_vision\\refactor\\image.png")
# print(url)