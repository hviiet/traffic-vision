from django.urls import re_path
from . import image_consumers, client_consumers, ws_consumer

websocket_urlpatterns = [
    re_path(r"^image/$", image_consumers.ImageConsumer.as_asgi()),
    re_path(r"^client/$", client_consumers.ClientConsumer.as_asgi()),
    re_path(r"^ws/$", ws_consumer.WSConsumer.as_asgi()),
]
