from django.views.decorators import gzip
from django.http import StreamingHttpResponse, HttpResponse 
import time
from PIL import Image
import io
from . import image_consumers

def get_image(request):
    try:
        image_data = open('image.png', 'rb').read()
        return HttpResponse(image_data, content_type="image/png")
    except Exception as e:
        print(e)

@gzip.gzip_page
def stream_image(request):
    try:
        response = StreamingHttpResponse(image_stream(), content_type='multipart/x-mixed-replace; boundary=frame')
        return response
    except Exception as e:
        print(e)

def convert_png_to_jpeg_bytes(png_image_path):
    # Open the PNG image
    # with Image.open(png_image_path) as image:
    #     jpeg_image_buffer = io.BytesIO()
    #     image.save(jpeg_image_buffer, format='JPEG')
    #     jpeg_image_bytes = jpeg_image_buffer.getvalue()
    
    # with open("image.png", "rb") as image:
    #     f = image.read()
    #     jpeg_image_bytes = bytearray(f)

    # return jpeg_image_bytes
    return image_consumers.ImageConsumer.frame

def image_stream():
    while True:
        try:
            # image_data = open('image.png', 'rb').read()
            # with open("image.png", "rb") as f:
            #     image_bytes = f.read()
            # image = Image.open(BytesIO(image_bytes))
            # img_io = BytesIO()
            # image.save(img_io, format='JPEG')
            # # img_io.seek(0)
            # img_bytes = img_io.read()
            # img_bytes = Image.open('image.png').tobytes()

            image_bytes = convert_png_to_jpeg_bytes('image.png')
            yield (b'--frame\r\n'
                b'Content-Type: image/jpeg\r\n\r\n' + image_bytes + b'\r\n')
        except Exception as e:
            print(e)

        # yield b'Content-Type: image/jpeg\r\n\r\n' + image_data + b'\r\n'
        # time.sleep(1)  # Adjust the delay based on your requirements


        # with open("image.png", "rb") as img:
        #     yield headers.encode('latin1') + img.read() + b"\r\n"
        # time.sleep(1)  # Delay for demonstration; adjust as needed