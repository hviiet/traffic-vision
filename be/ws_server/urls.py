from django.urls import path, re_path
from .views import stream_image, get_image

urlpatterns = [
    path('stream/', stream_image, name='stream-image'),
    re_path(r'^image/', get_image, name='get-image'),
]