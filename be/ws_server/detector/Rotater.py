"""
This class will find four vertices of a rectangle in a given image contains a picture of tilted rectangle in 3D space.
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt
import itertools

def read_img(path: str) -> np.ndarray:
    """
    Read the image and return the numpy array of the image.
    """
    path = "sign__test\\" + path
    img = cv2.imread(path)
    return img

def maximizeContrast(imgGrayscale):
    #Làm cho độ tương phản lớn nhất 
    height, width = imgGrayscale.shape
    
    imgTopHat = np.zeros((height, width, 1), np.uint8)
    imgBlackHat = np.zeros((height, width, 1), np.uint8)
    structuringElement = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)) #tạo bộ lọc kernel
    
    imgTopHat = cv2.morphologyEx(imgGrayscale, cv2.MORPH_TOPHAT, structuringElement, iterations = 15) #nổi bật chi tiết sáng trong nền tối
    imgBlackHat = cv2.morphologyEx(imgGrayscale, cv2.MORPH_BLACKHAT, structuringElement, iterations = 15) #Nổi bật chi tiết tối trong nền sáng
    imgGrayscalePlusTopHat = cv2.add(imgGrayscale, imgTopHat) 
    imgGrayscalePlusTopHatMinusBlackHat = cv2.subtract(imgGrayscalePlusTopHat, imgBlackHat)

    #Kết quả cuối là ảnh đã tăng độ tương phản 
    return imgGrayscalePlusTopHatMinusBlackHat

def preprocess(img: np.ndarray, plot: bool) -> np.ndarray:
    """
    Preprocess the image to make it easier to find the vertices of rectangle.
    """
    # resize and keep the aspect ratio, height of 200   
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # increase contrast
    img = maximizeContrast(img)
    _, img = cv2.threshold(img, 150, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    
    rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    img = cv2.dilate(img, rect_kernel, iterations = 1)
    
    # show 
    if plot:
        plt.imshow(img, cmap='gray')
        plt.show()
    edges = cv2.Canny(img, 50, 150)
    edges = cv2.dilate(edges, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
    
    # plot
    if plot:
        plt.imshow(edges, cmap='gray')
        plt.show()
    return edges

def extract_convex(img: np.ndarray, edges: np.ndarray, plot: bool, area_thresh = 0.5) -> np.ndarray:
    # Blank canvas.
    con = np.zeros_like(img)
    # Finding contours for the detected edges.
    contours, hierarchy = cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    # Keeping only the largest detected contour.
    page = sorted(contours, key=cv2.contourArea, reverse=True)[:5]
            
    # Blank canvas.
    con = np.zeros_like(img)
    # Loop over the contours.
    list_corners = []
    for c in page:
        # Approximate the contour.
        epsilon = 0.02 * cv2.arcLength(c, True)
        list_corners.append(cv2.approxPolyDP(c, epsilon, True))
    hull_areas = []
    alike_score = []
    for corners in list_corners:
        cv2.drawContours(con, corners, -1, (0, 255, 0), 10)
        # Sorting the corners and converting them to desired shape.
        corners = sorted(np.concatenate(corners).tolist())
        area, _, score = rectangle_score(corners, reduce_corners(corners))
        hull_areas.append(area)
        alike_score.append(score)
        # print("Area: ", area, "\tScore: ", score, "\tPercent:", area / img.shape[0] / img.shape[1] * 100)
        # Displaying the corners.
        for index, c in enumerate(corners):
            character = chr(65 + index)
            cv2.putText(con, character, tuple(c), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1, cv2.LINE_AA)
        if plot:
            plt.imshow(con)
            plt.show()
            # reset the canvas.
            con = np.zeros_like(img)
            
    # Finding the largest rectangle.
    indx = -1
    max_alike = 0
    for i, score in enumerate(alike_score):
        if score > max_alike and hull_areas[i] > area_thresh * img.shape[0] * img.shape[1]:
            max_alike = score
            indx = i
    if indx == -1:
        indx = np.argmax(alike_score)
    extracted_corners = list_corners[indx]
    # Sorting the corners and converting them to desired shape.
    corners = sorted(np.concatenate(extracted_corners).tolist())
    # Displaying the corners.
    if plot:
        for index, c in enumerate(corners):
            character = chr(65 + index)
            cv2.putText(con, character, tuple(c), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1, cv2.LINE_AA)
        plt.imshow(con)
        plt.show()
    
    return np.array(corners)

def reduce_corners(corners: np.ndarray) -> np.ndarray:
    MAX = 1000000
    # find four corners
    tl = [MAX, MAX]
    tr = [0, MAX]
    br = [0, 0]
    bl = [MAX, 0]
    for corner in corners:
        if corner[0] + corner[1] < tl[0] + tl[1]:
            tl = corner
        if corner[0] - corner[1] > tr[0] - tr[1]:
            tr = corner
        if corner[0] + corner[1] > br[0] + br[1]:
            br = corner
        if corner[0] - corner[1] < bl[0] - bl[1]:
            bl = corner
    return np.array([tl, tr, br, bl])

def add_margin(corners: np.ndarray, height: int, width: int, ratio: float) -> np.ndarray:
    rh = int(ratio * height)
    rw = int(ratio * width)
    [tl, tr, br, bl] = corners
    tl = [tl[0] - rw, tl[1] - rh]
    tr = [tr[0] + rw, tr[1] - rh]
    br = [br[0] + rw, br[1] + rh]
    bl = [bl[0] - rw, bl[1] + rh]
    # check if the corners are within the image.
    tl[0] = max(0, tl[0])
    tl[1] = max(0, tl[1])
    tr[0] = min(width, tr[0])
    tr[1] = max(0, tr[1])
    br[0] = min(width, br[0])
    br[1] = min(height, br[1])
    bl[0] = max(0, bl[0])
    bl[1] = min(height, bl[1])
    return np.array([tl, tr, br, bl])

def find_destination_corners(img: np.ndarray, corners: np.ndarray, plot: bool) -> np.ndarray:
    [tl, tr, br, bl] = corners
    # add margin
    if len(img.shape) == 2:
        height, width = img.shape
    else:
        height, width, _ = img.shape
    exp_corners = add_margin(corners, height, width, 0.00)
    # Finding the maximum width.
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    # Finding the maximum height.
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    # Final destination co-ordinates.
    destination_corners = [[0, 0], [maxWidth, 0], [maxWidth, maxHeight], [0, maxHeight]]
        
    # Getting the homography.
    M = cv2.getPerspectiveTransform(np.float32(exp_corners), np.float32(destination_corners))
    # Perspective transform using homography.
    rot = np.zeros_like(img)
    final = cv2.warpPerspective(img, M, (destination_corners[2][0], destination_corners[2][1]), flags=cv2.INTER_LINEAR, dst=rot)
    if plot:
        plt.imshow(final)
        plt.show()
    return final
    
# TODO: Implement this function.
def rectangle_score(corners: np.ndarray, quad_corners: np.ndarray) -> float:
    """
    This function will return the score of the rectangle based on the angle it creates.
    Range: 0 to 1
    """
    score = 0.0
    full_area = cv2.contourArea(cv2.convexHull(np.array(corners), False))
    quad_area = cv2.contourArea(cv2.convexHull(np.array(quad_corners), False))
    try:
        score = quad_area / full_area
    except:
        return 0, 0, 0
    return full_area, quad_area, score

def mask_outside(img: np.ndarray, convex: np.ndarray, plot: bool):
    """
    Fill outside of the convex with white.
    """
    # create a convex hull
    hull = cv2.convexHull(convex)
    mask = np.zeros_like(img)
    cv2.fillPoly(mask, [hull], (255, 255, 255))

    # Create an inverse mask
    mask_inv = cv2.bitwise_not(mask)

    # Create a white image
    white_img = np.ones_like(img) * 255

    # Apply the inverse mask to the white image
    outside_white = cv2.bitwise_and(white_img, mask_inv)

    # Combine the original image with the white outside
    result = cv2.bitwise_or(img, outside_white)

    if plot:
        plt.imshow(result)
        plt.show()

    return result
    
def tune(img: np.ndarray) -> np.ndarray:
    use_plot = False
    h, w, _ = img.shape
    img = cv2.resize(img, (int(w * 200 / h), 200))
    edges = preprocess(img, use_plot)
    convex = extract_convex(img, edges, use_plot, 0.3)
    img = mask_outside(img, convex, use_plot)
    convex = reduce_corners(convex)
    streched = find_destination_corners(img, convex, use_plot)
    return streched

# if __name__ == "__main__":
#     done = tune(cv2.imread("sign__test\\s2 (Large).png"))
