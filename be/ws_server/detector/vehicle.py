# enum type
from enum import Enum

# type : car, truck, bus, motorcycle
class Vehicle_Type(Enum):
    CAR = 2
    MOTORCYCLE = 3
    BUS = 5
    TRUCK = 7

class Vehicle_State(Enum):
    VIOLATE = 1
    NORMAL = 2

class Vehicle:
    def __init__(self):
        self.type = None
        self.id = None
        self.x1 = None
        self.y1 = None
        self.x2 = None
        self.y2 = None
        self.area = None
        self.maximize_area = None
        self.license_plate = None
        self.state = Vehicle_State.NORMAL
        self.license_plate_detected = False

    def __init__(self, type, id, boxes):
        self.type = type
        self.id = id
        self.x1 = boxes[0]
        self.y1 = boxes[1]
        self.x2 = boxes[2]
        self.y2 = boxes[3]
        self.area = (self.x2 - self.x1) * (self.y2 - self.y1)
        self.license_plate_detected = False
        self.license_plate = None
        self.state = Vehicle_State.NORMAL
        self.maximize_area = self.area


    def license_plate(self, license_plate):
        self.license_plate = license_plate

    def set_state(self, state):
        self.state = state

    def update_box(self, boxes):
        self.x1 = boxes[0]
        self.y1 = boxes[1]
        self.x2 = boxes[2]
        self.y2 = boxes[3]
        self.area = (self.x2 - self.x1) * (self.y2 - self.y1)
        if self.area > self.maximize_area:
            self.maximize_area = self.area

    # print vehicle information, including type, id, box
    def __str__(self):
        return "Vehicle type: %s, id: %d, box: (%d, %d, %d, %d)" % (self.type, self.id, self.x1, self.y1, self.x2, self.y2)
    


    