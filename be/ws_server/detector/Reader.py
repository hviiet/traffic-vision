import os
import pytesseract as pyt
import numpy as np
import cv2
from PIL import Image
# import Rotater as rt
# from .Rotater import read_img, maximizeContrast, preprocess, extract_convex, reduce_corners, add_margin, find_destination_corners, rectangle_score
from .Rotater import tune, maximizeContrast
import matplotlib.pyplot as plt

class Reader:
    WHITE_LIST = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-."
    TESSERACT_PATH = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"
    ALP_CONFIG = r'-c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZ.- -l eng --oem 3 --psm 6 language_model_penalty_non_freq_dict_word=1 -c language_model_penalty_non_dict_word=1 -c load_system_dawg=0'
    NUM_CONFIG = r'-c tessedit_char_whitelist=0123456789.- -l eng --oem 3 --psm 6 language_model_penalty_non_freq_dict_word=1 -c language_model_penalty_non_dict_word=1 -c load_system_dawg=0'
    FUL_CONFIG = r'-c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ.- -l eng --oem 3 --psm 6 -c language_model_penalty_non_dict_word=1 -c load_system_dawg=0'

    pyt.pytesseract.tesseract_cmd = TESSERACT_PATH
    
    def __init__(self):
        print("Initialized Reader")
    
    def set_path(path):
        if os.path.exists:
            Reader.TESSERACT_PATH = path
            pyt.pytesseract.tesseract_cmd = path
        else:
            print("Path does not exist")
    
    def get_path():
        return Reader.TESSERACT_PATH
        
    def read(self, img: np.ndarray, plot = False):
        img = tune(img)
        if len(img.shape) > 2:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            
        h, w = img.shape
        img = cv2.resize(img, (int(w * 400 / h), 400))
        
        # Threshold the image to binary
        img = maximizeContrast(img)
        _, img = cv2.threshold(img, 150, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        
        # apply erosion and dilation
        img = cv2.erode(img, (3, 3), iterations = 2)
        img = cv2.dilate(img, (3, 3), iterations = 2)
    
        # Apply morphological operations
        # rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        # img = cv2.dilate(img, rect_kernel, iterations = 0)
        
        img = 255 - img
        gray_pil = Image.fromarray(img)
        
        # Use pytesseract to read the image
        if plot:
            points = pyt.image_to_boxes(gray_pil, config=Reader.FUL_CONFIG)
        text = pyt.image_to_string(gray_pil, config=Reader.FUL_CONFIG)
        

        # Draw the boxes around the text
        if plot:
            for point in points.splitlines():
                point = point.split(' ')
                x, y, w, h = int(point[1]), int(point[2]), int(point[3]), int(point[4])
                cv2.rectangle(img, (x, img.shape[0]-y), (w, img.shape[0]-h), (255, 255, 255), 1)
        
            # Display the image
            cv2.imshow("Image", img)
            cv2.waitKey(0)
        if (text == ""):
            return "[UNKN]: No text detected"
        text = text.replace("\n", "-")
        if (text[-1] == "-"):
            text = text[:-1]
        return text.replace("\n", "-")

    
# main
# import time
# if __name__ == "__main__":
#     start = time.time()
#     reader = Reader()
#     text = reader.read(cv2.imread("license_plate\Vehicle_Type.MOTORCYCLE_22_license_plate.jpg"), plot=True)
#     print("SCAN RESULT:", text)
#     print("Time taken: ", time.time() - start)