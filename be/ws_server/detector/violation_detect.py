from pathlib import Path
import cv2
import numpy as np
from ultralytics import YOLO
import time, threading , queue, datetime
from shapely.geometry import Polygon, box

from .vehicle import Vehicle
from .vehicle import Vehicle_State
from .vehicle import Vehicle_Type
from .traffic_light import Traffic_Light
from .traffic_light import Traffic_Light_State

from django.utils import timezone

from api.models import Violation, Violator
from api.serializers import ViolationSerializer

from helpers.imgur_upload import upload_image_from_local_path, upload_image_from_bytes

from .Reader import Reader

import os, shutil, math, time, asyncio

BASE_DIR = Path(__file__).resolve().parent
LICENSE_PLATE_PATH = os.path.join(BASE_DIR, 'license_plate')
VEHICLE_IMAGE_PATH = os.path.join(BASE_DIR, 'vehicle_images')
LOG_FILE_PATH = os.path.join(BASE_DIR, 'violated.txt')
CUDA_DEVICE = 'cuda:0'
VIOLATION_VALUE = 0.08
MAX_WIDTH = 640

class ViolationDetect:
    def __init__(self, location, send_data_to_group):
        self.location = location
        self.send_data_to_group = send_data_to_group
        # Global Variables
        self.video_path = os.path.join(BASE_DIR, 'test_vid', 'video_test1.MOV')
        self.minimize_size_allow_to_detect = {
            Vehicle_Type.CAR : 1/25,
            Vehicle_Type.BUS : 1/20,
            Vehicle_Type.TRUCK : 1/20,
            Vehicle_Type.MOTORCYCLE : 1/200,
        }
        self.model = YOLO(os.path.join(BASE_DIR, 'yolov8n.pt'), verbose=False)
        self.license_plate = YOLO(os.path.join(BASE_DIR, 'license_plate.pt'), verbose=False)
        self.transportation_coco_id = [2, 3, 5, 7]
        self.cap = cv2.VideoCapture(self.video_path)
        self.vehicles = []
        self.main_traffic_light = None
        self.setup_flag = True
        self.frame_stride = 1
        self.current_frame = 0
        self.clear_content()
        self.violated_area = self.location.get_violated_area_in_array()
        pass

    def __del__(self):
        self.cap.release()
        cv2.destroyAllWindows()

    async def test_send_function(self):
        path = "C:\\Users\\nviet\\Destop\\traffic_vision\\refactor\\image.png"
        url = upload_image_from_local_path(path)
        print(url)

    async def handle_frame(self, bytes_data):
        frame = cv2.imdecode(np.frombuffer(bytes_data, np.uint8), cv2.IMREAD_COLOR)

        # Copy an original frame to extract if needed
        frame_copy = frame.copy()

        # Resize frame
        new_width = MAX_WIDTH
        aspect_ratio = frame.shape[1] / frame.shape[0]
        new_height = int(new_width / aspect_ratio)
        frame = cv2.resize(frame, (new_width, new_height))

        if self.setup_flag:
            # self.setup(frame)
            self.main_traffic_light = self.setup(frame) 
            if (self.main_traffic_light != None):
                self.setup_flag = False
            return

        if self.current_frame % self.frame_stride == 0:
            results = self.model.track(frame, device=CUDA_DEVICE, stream=False, persist=True, classes=self.transportation_coco_id)
    
            # Get boxes, labels, and track_ids of detected vehicles from the results
            boxes = results[0].boxes
            labels = boxes.cls
            track_ids = boxes.id
            if (labels != None):
                labels = labels.int().cpu().tolist()
            else:
                labels = []
            if (track_ids != None):
                track_ids = track_ids.int().cpu().tolist()
            else:
                track_ids = []

            existing_vehicle_ids = [vehicle.id for vehicle in self.vehicles]
            if (len(track_ids) == len(labels)):
                for i in range(len(labels)):
                    # Add new vehicle to the list
                    if not track_ids[i] in existing_vehicle_ids:
                        new_vehicle_id = track_ids[i]
                        new_box = boxes[i].xyxyn.cpu().tolist()
                        vehicle_type = Vehicle_Type(labels[i])
                        new_vehicle = Vehicle(vehicle_type, new_vehicle_id, new_box[0])
                        if new_vehicle.type in self.minimize_size_allow_to_detect:
                            if new_vehicle.area < self.minimize_size_allow_to_detect[new_vehicle.type]:
                                continue
                        self.vehicles.append(new_vehicle)
                    # Update boxes of existing vehicle in the list
                    else:
                        for existing_id in existing_vehicle_ids:
                            if existing_id == track_ids[i]:
                                index = existing_vehicle_ids.index(existing_id)
                                self.vehicles[index].update_box(boxes[i].xyxyn[0].cpu().tolist())

                    # Remove vehicle from the list if it is not in the tracking list anymore
                    for existing_id in existing_vehicle_ids:
                        if existing_id not in track_ids:
                            index = existing_vehicle_ids.index(existing_id)
                            self.vehicles.pop(index)
                            existing_vehicle_ids.pop(index)
                            
            self.extract_traffic_light(frame, self.main_traffic_light)
            await self.check_violation(self.vehicles, self.main_traffic_light, bytes_data)
            frame_copy = self.draw_violated_box(frame_copy, self.vehicles, self.main_traffic_light)

            # Detect license plate of vehicles in the frame
            self.extract_vehicles(frame_copy, self.vehicles, self.main_traffic_light)

            #annotated_frame = results[0].plot()

            self.current_frame += 1
        else:
            self.current_frame += 1

    def setup(self, frame):
        print ('Setting things up...')
        traffic_lights = []

        result = self.model(frame, device=CUDA_DEVICE, classes=[9], conf=0.15)

        boxes = result[0].boxes
        
        for i in range(len(boxes)):
            box = boxes[i].xyxy[0].int().cpu().tolist()
            x1, y1, x2, y2 = box[0], box[1], box[2], box[3]
            traffic_light = Traffic_Light(i, x1, y1, x2, y2)
            traffic_lights.append(traffic_light)

        max_area = 0
        main_traffic_light = None
        for traffic_light in traffic_lights:
            if traffic_light.area > max_area:
                max_area = traffic_light.area
                main_traffic_light = traffic_light
        return main_traffic_light

    def polygon_area(self,points):
        area = 0
        for i in range(len(points) - 1):
            x1, y1 = points[i]
            x2, y2 = points[i + 1]
            area += x1 * y2 - x2 * y1
        x1, y1 = points[-1]
        x2, y2 = points[0]
        area += x1 * y2 - x2 * y1
        return abs(area / 2)

    def rectangle_area(self,x1, y1, x2, y2):
        return (x2 - x1) * (y2 - y1)

    def is_intersecting(self,polygon, rectangle):
        poly = Polygon(polygon)
        rect = box(rectangle[0], rectangle[1], rectangle[2], rectangle[3])
        return poly.intersects(rect)

    def intersection_area(self,polygon, rectangle):
        poly = Polygon(polygon)
        rect = box(rectangle[0], rectangle[1], rectangle[2], rectangle[3])
        intersection = poly.intersection(rect)
        return intersection.area

    def calculate_iou(self, violated_area, vehicle):
        polygon = violated_area
        rectangle = [vehicle.x1, vehicle.y1, vehicle.x2, vehicle.y2]
        polygon_area_value = self.polygon_area(polygon)
        rectangle_area_value = self.rectangle_area(rectangle[0], rectangle[1], rectangle[2], rectangle[3])

        if not self.is_intersecting(polygon, rectangle):
            return 0

        intersection_area_value = self.intersection_area(polygon, rectangle)

        iou = intersection_area_value / (polygon_area_value + rectangle_area_value - intersection_area_value)
        return iou

    def extract_vehicles(self, frame, vehicles, traffic_light):
        for vehicle in vehicles:
            # Check if the vehicle meets the minimum size requirement for license plate detection
            if vehicle.type in self.minimize_size_allow_to_detect:
                if vehicle.area < self.minimize_size_allow_to_detect[vehicle.type]:
                    continue

            # Check if license plate has already been detected or if the vehicle has reached maximum area
            if not vehicle.license_plate_detected and vehicle.area == vehicle.maximize_area:
                if (vehicle.y2 >0.98):
                    vehicle.maximize_area = 0
                    continue
                if (vehicle.y1 < 0.3):
                    continue
                if (vehicle.x1 < 0.3):
                    continue

                x1, y1, x2, y2 = vehicle.x1, vehicle.y1, vehicle.x2, vehicle.y2

                vehicle_img = frame[int(y1 * frame.shape[0]):int(y2 * frame.shape[0]), int(x1 * frame.shape[1]):int(x2 * frame.shape[1])]

                # Save the vehicle image to disk with name: vehicle-type + vehicle-id
                # cv2.imwrite(VEHICLE_IMAGE_PATH + str(vehicle.type) + '_' + str(vehicle.id) + '.jpg', vehicle_img)
                cv2.imwrite(os.path.join(VEHICLE_IMAGE_PATH, str(vehicle.type) + '_' + str(vehicle.id) + '.jpg'), vehicle_img)

    def extract_license_plate(self, vehicle):
                if vehicle.state == Vehicle_State.VIOLATE:
                    # Detect license plate
                    # vehicle_img = cv2.imread(VEHICLE_IMAGE_PATH + str(vehicle.type) + '_' + str(vehicle.id) + '.jpg')
                    vehicle_img = cv2.imread(os.path.join(VEHICLE_IMAGE_PATH, str(vehicle.type) + '_' + str(vehicle.id) + '.jpg'))
                    result = self.license_plate(vehicle_img, device=CUDA_DEVICE, conf = 0.5)
                    box = result[0].boxes

                    # If license plate is detected by model
                    if len(box) > 0:
                        vehicle.license_plate_detected = True
                        box = box[0].xyxy[0].int().cpu().tolist()

                        # Save license plate image to disk with name: vehicle-type + vehicle-id + license_plate.jpg
                        license_plate_img = vehicle_img[box[1]:box[3], box[0]:box[2]]
                        # cv2.imwrite(LICENSE_PLATE_PATH + str(vehicle.type) + '_' + str(vehicle.id) + '_license_plate.jpg', license_plate_img)
                        cv2.imwrite(os.path.join(LICENSE_PLATE_PATH, str(vehicle.type) + '_' + str(vehicle.id) + '_license_plate.jpg'), license_plate_img)

    def extract_traffic_light(self,frame, traffic_light):
        traffic_img = frame[traffic_light.y1:traffic_light.y2, traffic_light.x1:traffic_light.x2]
        traffic_light.determine_state(traffic_img)
        # crop and save the traffic light image to disk
        # cv2.imwrite(str(traffic_light.id) + '.jpg', traffic_img)
        cv2.imwrite(os.path.join(BASE_DIR, str(traffic_light.id) + '.jpg'), traffic_img)

    # Check and Write VIOLATION
    async def check_violation(self, vehicles, traffic_light, bytes_data):
        if traffic_light.state == Traffic_Light_State.RED:
            for vehicle in vehicles:
                if vehicle.state  != Vehicle_State.VIOLATE:
                    # if the vehicle box is IOU with the violated area box greater than 0.3 then the vehicle is violated
                    if self.calculate_iou(self.violated_area, vehicle) > VIOLATION_VALUE:
                        vehicle.state = Vehicle_State.VIOLATE
                        self.extract_license_plate(vehicle)
                        self.read_license_plate(vehicle)
                        with open(LOG_FILE_PATH, 'a') as f:
                            f.write('Vehicle ' + str(vehicle.id) + ' violated the traffic light\n')
                            f.write('Directory: vehicle_images/' + str(vehicle.type) + '_' + str(vehicle.id) + '.jpg\n')
                            f.write('License plate: ' + vehicle.license_plate + '\n')
                            f.write('\n')
                        # Create a new thread to upload the image and send the data to the client
                        threading.Thread(target=asyncio.run, args=(self.upload_image_and_send_data(vehicle, bytes_data),)).start()

    async def upload_image_and_send_data(self, vehicle, bytes_data):
        # Upload the image to imgur
        url = upload_image_from_bytes(bytes_data)
        vehicle_type = str(vehicle.type).split('.')[1]
        # Get the violator from the database, if not exist then create a new one
        violator, created = await Violator.objects.aget_or_create(vehicle_type=vehicle_type, vehicle_plate=vehicle.license_plate)
        # Save the violation to the database
        violation = await Violation.objects.acreate(location=self.location, violator=violator,
                                                    violation_image_path=url, violation_time=datetime.datetime.now())
        # Send the violation data to the client
        violation_serializer = ViolationSerializer(violation)
        await self.send_data_to_group(violation_serializer.data)

    def draw_violated_box(self,frame, vehicles, traffic_light):
        if traffic_light.state == Traffic_Light_State.RED:
            for vehicle in vehicles:
                if vehicle.state == Vehicle_State.VIOLATE:
                    x1, y1, x2, y2 = vehicle.x1, vehicle.y1, vehicle.x2, vehicle.y2
                    cv2.rectangle(frame, (int(x1 * frame.shape[1]), int(y1 * frame.shape[0])), (int(x2 * frame.shape[1]), int(y2 * frame.shape[0])), (0, 0, 255), 2)
        return frame

    def read_license_plate(self,vehicle):
        # license_plate_img = cv2.imread(LICENSE_PLATE_PATH + str(vehicle.type) + '_' + str(vehicle.id) + '_license_plate.jpg')
        license_plate_img = cv2.imread(os.path.join(LICENSE_PLATE_PATH, str(vehicle.type) + '_' + str(vehicle.id) + '_license_plate.jpg'))
        reader = Reader()
        vehicle.license_plate = reader.read(license_plate_img, plot=False)

    def crop_first_frame_to_setup(self):
        cap = cv2.VideoCapture(self.video_path)
        success, frame = cap.read()
        if success:
            #resize frame
            new_width = 1000
            aspect_ratio = frame.shape[1] / frame.shape[0]
            new_height = int(new_width / aspect_ratio)
            frame = cv2.resize(frame, (new_width, new_height))
            cv2.imwrite(os.path.join(BASE_DIR, 'setup.jpg'), frame)
        else:
            print('Failed to capture first frame')
        cap.release()
        cv2.destroyAllWindows()

    async def select_area(self, bytes_data=None):
        if bytes_data == None:
            self.crop_first_frame_to_setup()
            # Load the image
            image = cv2.imread(os.path.join(BASE_DIR, 'setup.jpg'))
        else:
            image = cv2.imdecode(np.frombuffer(bytes_data, np.uint8), cv2.IMREAD_COLOR)
            # resize frame
            new_width = 500
            aspect_ratio = image.shape[1] / image.shape[0]
            new_height = int(new_width / aspect_ratio)
            image = cv2.resize(image, (new_width, new_height))

        # Create a window and display the image
        cv2.namedWindow("Select Area")
        cv2.imshow("Select Area", image)

        # Create a list to store the selected points
        points = []

        # Mouse callback function to handle mouse events
        def mouse_callback(event, x, y, flags, param):
            if event == cv2.EVENT_LBUTTONDOWN:
                # Add the selected point to the list
                points.append((x, y))

                # Draw a circle at the selected point
                cv2.circle(image, (x, y), 5, (0, 255, 0), -1)

                # Display the updated image
                cv2.imshow("Select Area", image)

                # Check if four points have been selected
                if len(points) == 4:
                    # Close the window
                    cv2.destroyAllWindows()

        # Set the mouse callback function
        cv2.setMouseCallback("Select Area", mouse_callback)

        # Wait for the user to select four points
        while len(points) < 4:
            cv2.waitKey(1)

        # Return the selected points, norm 0-1
        selected_points = [(x / image.shape[1], y / image.shape[0]) for x, y in points]
        selected_points_string = ','.join(f"{x[0]},{x[1]}" for x in selected_points)
        self.location.violated_area = selected_points_string
        await self.location.asave()
        self.violated_area = selected_points
        return selected_points

    def clear_content(self):
        folder = LICENSE_PLATE_PATH
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))

        # remove all file in vehicle_images folder
        folder = VEHICLE_IMAGE_PATH
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))

        f = open(LOG_FILE_PATH, "w")
        f.write("")
        f.close()

    








