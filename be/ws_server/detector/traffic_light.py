from enum import Enum

import cv2
import numpy as np

class Traffic_Light_State(Enum):
    RED = 1
    YELLOW = 2
    GREEN = 3
    OFF = 4

class Traffic_Light:
    def __init__(self):
        self.state = None
        self.id = None
        self.x1 = None
        self.y1 = None
        self.x2 = None
        self.y2 = None
        self.detected = False
        self.area = None
    def __init__(self,id=0, x1=0, y1=0, x2=0, y2=0):
        self.state = Traffic_Light_State.OFF
        self.id = id
        self.x1 = x1 - 5
        self.y1 = y1 - 5
        self.x2 = x2 + 5
        self.y2 = y2 + 5
        self.detected = True
        self.area = (self.x2 - self.x1) * (self.y2 - self.y1)

    def set_state(self, state):
        self.state = state
    def set_id(self, id):
        self.id = id
    def get_state(self):
        return self.state
    def get_id(self):
        return self.id
    def __str__(self):
        return "Traffic Light: " + self.id + " is " + self.state
    
    # From an image of traffic light, determine the state of the traffic light
    def determine_state(self, image):
        # Check if image is a numpy array
        if not isinstance(image, np.ndarray):
            image = cv2.imread(image)

        # Convert the image to HSV color space
        hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

        # Define the lower and upper bounds for the colors of traffic lights
        # These values might need to be adjusted based on the specific conditions and lighting
        lower_red1 = np.array([0, 100, 100])
        upper_red1 = np.array([10, 255, 150])

        lower_red2 = np.array([165, 100, 100])
        upper_red2 = np.array([179, 255, 150])

        lower_yellow = np.array([12, 100, 100])
        upper_yellow = np.array([35, 255, 255])

        lower_green = np.array([50, 100, 100])
        upper_green = np.array([90, 255, 255])

        # Threshold the HSV image to get only the desired colors
        mask_red1 = cv2.inRange(hsv_image, lower_red1, upper_red1)
        mask_red2 = cv2.inRange(hsv_image, lower_red2, upper_red2)
        mask_yellow = cv2.inRange(hsv_image, lower_yellow, upper_yellow)
        mask_green = cv2.inRange(hsv_image, lower_green, upper_green)

        filter_img_red1 = cv2.bitwise_and(image, image, mask=mask_red1)
        filter_img_red2 = cv2.bitwise_and(image, image, mask=mask_red2)
        filter_img_yellow = cv2.bitwise_and(image, image, mask=mask_yellow)
        filter_img_green = cv2.bitwise_and(image, image, mask=mask_green)

        nonzero_red1 = np.count_nonzero(filter_img_red1)
        nonzero_red2 = np.count_nonzero(filter_img_red2)
        nonzero_yellow = np.count_nonzero(filter_img_yellow)
        nonzero_green = np.count_nonzero(filter_img_green)

        # Determine the state of the traffic light based on the color with the most pixels
        if nonzero_red1 > nonzero_red2 and nonzero_red1 > nonzero_yellow and nonzero_red1 > nonzero_green:
            self.state = Traffic_Light_State.RED
        elif nonzero_red2 > nonzero_red1 and nonzero_red2 > nonzero_yellow and nonzero_red2 > nonzero_green:
            self.state = Traffic_Light_State.RED
        elif nonzero_yellow > nonzero_red1 and nonzero_yellow > nonzero_red2 and nonzero_yellow > nonzero_green:
            self.state = Traffic_Light_State.YELLOW
        elif nonzero_green > nonzero_red1 and nonzero_green > nonzero_red2 and nonzero_green > nonzero_yellow:
            self.state = Traffic_Light_State.GREEN
        else:
            self.state = Traffic_Light_State.OFF

        # If there are red and green pixels, the traffic light is likely red with right turn signal
        if abs(nonzero_red1 - nonzero_green) < 200 and (nonzero_red1 > 50):
            self.state = Traffic_Light_State.RED
        elif abs(nonzero_red2 - nonzero_green) < 200 and (nonzero_red2 > 50):
            self.state = Traffic_Light_State.RED

