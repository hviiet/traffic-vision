from django.apps import AppConfig


class WsServerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ws_server'
