import asyncio
import os
from channels.generic.websocket import AsyncWebsocketConsumer
from PIL import Image
from io import BytesIO

class ImageConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        await self.accept()
        print("Connected\n")

    async def disconnect(self, close_code):
        print("Disconnected\n")

    async def receive(self, text_data=None, bytes_data=None):
        if bytes_data:
            is_valid = await self.is_valid_image(bytes_data)
            if is_valid:
                os.makedirs('images', exist_ok=True)

                image_file_name = f"image.png"                

                with open(image_file_name, 'wb') as file:
                    file.write(bytes_data)


    async def is_valid_image(self, image_bytes):
        try:
            Image.open(BytesIO(image_bytes)).verify()
            return True
        except Image.UnidentifiedImageError:
            print("Image invalid")
            return False
