import asyncio
import json
from channels.generic.websocket import AsyncWebsocketConsumer
from api.models import Violation
from api.serializers import ViolationSerializer

class ClientConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        await self.accept()
        print("A new user connected\n")
        self.message_task = asyncio.create_task(self.send_messages_periodically())

    async def disconnect(self, close_code):
        print("A user has disconnected\n")
    
    async def send_messages_periodically(self):
        violation = await Violation.objects.select_related('location', 'violator').alast()
        if violation:
            while True:
                data = ViolationSerializer(violation)
                await self.send(text_data=json.dumps(data.data))
                await asyncio.sleep(10)

