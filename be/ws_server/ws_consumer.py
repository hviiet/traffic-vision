import asyncio, json, time, threading
from PIL import Image
from io import BytesIO
from channels.generic.websocket import AsyncWebsocketConsumer
from api.models import Violation, LocationInfo
from api.serializers import ViolationSerializer
from .detector.violation_detect import ViolationDetect
from helpers.imgur_upload import upload_image_from_bytes

CLIENT_GROUP_NAME = 'clients'

class WSConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        try:
            print(f"New client connecting\n")
            self.type = None
            await self.accept()
            # send the client the on_connect message
            await self.send(text_data=json.dumps({
                'type': 'on_connect',
            }))
        except Exception as e:
            print(f"Error: {e}")        

    async def disconnect(self, close_code):
        try:
            # Remove the client from the group 
            await self.channel_layer.group_discard(
                CLIENT_GROUP_NAME,
                self.channel_name
            )
            if self.type:
                if self.type == 'camera':
                    del self.detector
                    print(f"Camera disconnected\n")
                elif self.type == 'client':
                    print(f"Client disconnected\n")
        except Exception as e:
            print(f"Error: {e}")
    
    async def receive(self, text_data=None, bytes_data=None):
        try:
            if text_data:
                data = json.loads(text_data)
                # On Connection
                if 'connect_type' in data:
                    if data['connect_type'] == 'camera':
                        camera_name = data['camera_name']
                        address = data['address']
                        # location = await LocationInfo.objects.aget(camera_name=camera_name)
                        location, created = await LocationInfo.objects.aget_or_create(camera_name=camera_name, address=address)
                        self.type = 'camera'
                        self.detector = ViolationDetect(location, self.send_data_to_group)

                        # print("start test")
                        # await self.detector.test_send_function()
                        # threading.Thread(target=asyncio.run, args=(self.detector.test_send_function(),)).start()
                        # print("end test")

                        print(f"Camera connected\n")
                        if self.detector.violated_area is None or "first_frame" in data:
                            self.is_setup = False 
                            await self.send(text_data=json.dumps({
                                'type': 'send_first_frame',
                            }))
                        else:
                            self.is_setup = True
                            print("Setup already done, loaded from database.")
                            await self.send(text_data=json.dumps({
                                'type': 'sent_video',
                            }))

                    elif data['connect_type'] == 'client':
                        self.type = 'client'
                        await self.channel_layer.group_add(
                            CLIENT_GROUP_NAME,
                            self.channel_name
                        )
                        print(f"Client connected\n")
                else:
                    if self.type == 'camera':
                        # send the data to the client
                        await self.channel_layer.group_send(
                            CLIENT_GROUP_NAME,
                            {
                                'type': 'send_data',
                                'data': json.dumps(data)
                            }
                        )
                    elif self.type == 'client':
                        pass
            elif bytes_data:
                    if self.is_setup:
                        is_valid = await self.is_valid_image(bytes_data)
                        if is_valid:
                            image_file_name = "image.png"
                            with open(image_file_name, 'wb') as file:
                                file.write(bytes_data)
                            execute_time = time.time()
                            await self.detector.handle_frame(bytes_data)
                            print(f"1 frame execute time: {time.time() - execute_time}")
                    else:
                        self.is_setup = True
                        await self.detector.select_area(bytes_data)
                        print("Setup done")
                        await self.send(text_data=json.dumps({
                            'type': 'sent_video',
                        }))
        except Exception as e:
            print(f"Error: {e}")


    async def send_data_to_group(self, data, group_name = CLIENT_GROUP_NAME):
        try:
            await self.channel_layer.group_send(
                group_name,
                {
                    'type': 'send_data',
                    'data': json.dumps(data)
                }
            )
        except Exception as e:
            print(f"Error: {e}")
    async def send_data(self, event):
        data = event['data']
        await self.send(text_data=data)

    async def send_messages_periodically(self):
        violation = await Violation.objects.select_related('location', 'violator').alast()
        if violation:
            while True:
                data = ViolationSerializer(violation)
                await self.send(text_data=json.dumps(data.data))
                await asyncio.sleep(10)

    async def is_valid_image(self, image_bytes):
        try:
            Image.open(BytesIO(image_bytes)).verify()
            return True
        except Image.UnidentifiedImageError:
            print("Image invalid")
            return False