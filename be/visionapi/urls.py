from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', include('ws_server.urls')),
    path('api/', include('api.urls')),
]