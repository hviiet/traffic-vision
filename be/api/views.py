from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Violation
from .serializers import *

# Get Violation Data with Pagination
@api_view(['POST'])
def get_violation_data(request):
    if request.method == 'POST':
        request_data = request.data
        page = int(request_data['page'] if 'page' in request_data else 1)
        page_size = int(request_data['page_size'] if 'page_size' in request_data else 10)
        page_size = min(25, page_size)
        plate_number = request_data['plate_number'] if 'plate_number' in request_data else None
        if plate_number:
            violations  = Violation.objects.filter(violator__vehicle_plate=plate_number).order_by('violation_time')
        else:
            violations  = Violation.objects.all().order_by('violation_time')

        paginator = Paginator(violations , page_size)

        try:
            violations  = paginator.page(page)
        except EmptyPage:
            violations  = paginator.page(paginator.num_pages)

        serializer = ViolationSerializer(violations , context={'request': request}, many=True)

        return Response({
            'total_pages': paginator.num_pages,
            'current_page': page,
            'page_size': page_size,
            'total_records': paginator.count,
            'violations': serializer.data,
        }, status=status.HTTP_200_OK)