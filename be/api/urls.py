from django.urls import path
from . import views

urlpatterns = [
    path('violations/', views.get_violation_data),
]