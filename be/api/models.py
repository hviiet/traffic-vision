from django.db import models

class LocationInfo(models.Model):
    id = models.AutoField(primary_key=True)
    address = models.CharField(max_length=560)
    camera_name = models.CharField(max_length=240)
    violated_area = models.CharField(max_length=240, default=None, null=True)

    def __str__(self):
        return f"{self.id} - {self.address} - {self.camera_name} - {self.violated_area}"
    
    def get_violated_area_in_array(self):
        try:
            if self.violated_area is None:
                return None
            selected_points_string_decode = []
            splitted = self.violated_area.split(',')
            for i in range(0, len(splitted), 2):
                x = float(splitted[i])
                y = float(splitted[i + 1])
                selected_points_string_decode.append((x, y))
            if len(selected_points_string_decode) < 3:
                return None
            return selected_points_string_decode
        except:
            return None

        

class Violator(models.Model):
    id = models.AutoField(primary_key=True)
    vehicle_type = models.CharField(max_length=240)
    vehicle_plate = models.CharField(max_length=240)

    def __str__(self):
        return f"{self.vehicle_type} - {self.vehicle_plate}"

class Violation(models.Model):
    id = models.AutoField(primary_key=True)
    violation_image_path = models.CharField(max_length=240)
    violation_time = models.DateTimeField()
    location = models.ForeignKey(LocationInfo, on_delete=models.CASCADE)
    violator = models.ForeignKey(Violator, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.violation_time} - {self.location} - {self.violator}"