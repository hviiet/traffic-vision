from rest_framework import serializers
from .models import LocationInfo, Violator, Violation

class LocationInfoSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = LocationInfo
        fields = ('address', 'camera_name')

class ViolatorSerializer(serializers.ModelSerializer):
        
    class Meta:
        model = Violator
        fields = ('vehicle_type', 'vehicle_plate')

class ViolationSerializer(serializers.ModelSerializer):
    location = LocationInfoSerializer()
    violator = ViolatorSerializer()
                
    class Meta:
        model = Violation
        fields = ('violation_image_path', 'violation_time', 'location', 'violator')