import cv2, websocket, threading, time, rel, json, sys

cap = cv2.VideoCapture("C:\\Users\\nviet\\Destop\\traffic_vision\\refactor\\test-1080p.mp4")
# cap = cv2.VideoCapture("C:\\Users\\nviet\\Destop\\traffic_vision\\refactor\\test-1080-zoom.mp4")
# cap = cv2.VideoCapture("C:\\Users\\nviet\\Destop\\traffic_vision\\refactor\\test-zoom.mp4")
# cap = cv2.VideoCapture("C:\\Users\\nviet\\Destop\\traffic_vision\\refactor\\video_test1.MOV")

def send_video():
    print("Start sending video")
    i = 0
    length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    width  = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps    = cap.get(cv2.CAP_PROP_FPS)
    print(f"Length: {length}, Width: {width}, Height: {height}, FPS: {fps}, Duration: {length/fps}")
    start_time = time.time()
    while cap.isOpened():
        # break
        frame_time = time.time()
        i += 1
        ret, frame = cap.read()
        if i % 2 != 0:
            continue
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break
            
        bytes_data = cv2.imencode('.jpg', frame)[1].tobytes()
        bytes_size = sys.getsizeof(bytes_data)
        
        ws.send(bytes_data, opcode=websocket.ABNF.OPCODE_BINARY)
        print(f"Frame {i} sent, size {bytes_size} time: {time.time() - frame_time}")
    cap.release()
    print(f"Send duration: {time.time() - start_time}")

def on_message(ws, message):
    data = json.loads(message)
    if data['type'] == "on_connect":
        ws.send(json.dumps({
            "connect_type": "camera",
            "camera_name": "CAM_01",
            "address": "Hòa Khánh Bắc - Liên Chiểu"
            # "first_frame": "True",
        }))
    elif data['type'] == "send_first_frame":
        ret, frame = cap.read()
        bytes_data = cv2.imencode('.png', frame)[1].tobytes()
        ws.send(bytes_data, opcode=websocket.ABNF.OPCODE_BINARY)
    elif data['type'] == "sent_video":
        threading.Thread(target=send_video).start()
        # print("Start sending video")
        # i = 0
        # length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        # width  = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        # height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        # fps    = cap.get(cv2.CAP_PROP_FPS)
        # print(f"Length: {length}, Width: {width}, Height: {height}, FPS: {fps}, Duration: {length/fps}")
        # start_time = time.time()
        # while cap.isOpened():
        #     # break
        #     frame_time = time.time()
        #     i += 1
        #     ret, frame = cap.read()
        #     if i % 2 != 0:
        #         continue
        #     if not ret:
        #         print("Can't receive frame (stream end?). Exiting ...")
        #         break
                
        #     bytes_data = cv2.imencode('.jpg', frame)[1].tobytes()
        #     bytes_size = sys.getsizeof(bytes_data)
        #     ws.send(bytes_data, opcode=websocket.ABNF.OPCODE_BINARY)
        #     print(f"Frame {i} sent, size {bytes_size} time: {time.time() - frame_time}")
        # cap.release()
        # print(f"Send duration: {time.time() - start_time}")
        # ws.close()

def on_error(ws, error):
    print(error)

def on_close(ws, close_status_code, close_msg):
    print("### closed ###")

def on_open(ws):
    print("Opened connection")


websocket.enableTrace(False)
ws = websocket.WebSocketApp("ws://127.0.0.1:8000/ws/",
                            on_open=on_open,
                            on_message=on_message,
                            on_error=on_error,
                            on_close=on_close)

ws.run_forever(dispatcher=rel, reconnect=5)  # Set dispatcher to automatic reconnection, 5 second reconnect delay if connection closed unexpectedly
rel.signal(2, rel.abort)  # Keyboard Interrupt
rel.dispatch()

# ws.run_forever(reconnect=5)
